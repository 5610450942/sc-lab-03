package model;

public class Hashing {
	public int sumAscii(String str){
		int sumAscii = 0;
		for (char ch:str.toCharArray() ){
			sumAscii += ch;
		}
		return sumAscii;
	} 
	public int modSum(int n,int mod){
		return n%mod;
	}
}
