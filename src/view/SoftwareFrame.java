package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.Formatter;
import java.util.StringTokenizer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class SoftwareFrame extends JFrame {
	private JPanel pannel1;
	private JPanel pannel3;
	private JPanel pannel2;
	private JTextArea showResult;
	private JTextArea strURL;
	private JButton endButton;
	private JButton showResultButton;
	private String str;
	public SoftwareFrame() {
	   createFrame();
	   
    }
    public void createFrame() { 
    	str = "sum ascii : hashing  \n";
    	pannel1 = new JPanel();
    	pannel2 = new JPanel();
    	pannel3 = new JPanel();
    	showResult = new JTextArea(str);
	    strURL = new JTextArea(12,20);
	    strURL.setLineWrap(true);
	    //showResult.setLineWrap(true);
	    endButton = new JButton("end program");
	    showResultButton = new JButton("show result");
	    
	    pannel1.add(strURL);
	    pannel2.add(showResult);
	    pannel3.add(showResultButton);
	    pannel3.add(endButton);
 
 	   setLayout(new BorderLayout());
 	   add(pannel1, BorderLayout.WEST);
 	   add(pannel2, BorderLayout.EAST);
 	   add(pannel3, BorderLayout.SOUTH);
 	   
 	   pannel1.setBorder(BorderFactory.createLineBorder(Color.red));
 	   pannel2.setBorder(BorderFactory.createLineBorder(Color.red));
 	   
    }

    public void setListenerEnd(ActionListener list) {
	    endButton.addActionListener(list);
    }
    public void setListenerShowResult(ActionListener list) {
	    showResultButton.addActionListener(list);
    }

    public StringTokenizer getTextURL() {
        StringTokenizer st = new StringTokenizer(strURL.getText(),"\n");
        return st;
    }
	public void setResult(int sum,int hashing) {
		str = sum+"\t"+hashing+"   "+"\n"+str+" \n" ;
		showResult.setText(str);
	}
	public void setDefaultResult() {
		str = "sum ascii : hashing  \n";
		showResult.setText(str);
	}
	public void setTextURL(String string) {
		// TODO Auto-generated method stub
		strURL.setText(string);
	}
	

} 
